# TLJH + Nbgrader

## Prerequisites

- An up to date Ubuntu server
  > I'm using Ubuntu 18.04 from [hetzner](https://console.hetzner.cloud/)

Update packages with

    apt update
    apt upgrade

### Firewall

If you are not using a private network, creating firewall rules is highly recommended.

Start by disabling HTTP connection to the server with `ufw`

    ufw allow 22/tcp
    ufw deny 80/tcp
    ufw disable
    ufw enable

> If the terminal hangs, exit the ssh session with
>
>     [ENTER]~.

Now you can check the firewall status with

    ufw status

## Installing TLJH

[Source](http://tljh.jupyter.org/en/latest/install/custom-server.html)

> Run the following commands as root

Install a C compiler

    apt install -y gcc python3-dev g++

Replace `<admin-user-name>` with the initial admin user and run the following command. The admin user can be reconfigured later

    curl https://raw.githubusercontent.com/jupyterhub/the-littlest-jupyterhub/master/bootstrap/bootstrap.py \
    | python3 - --admin <admin-user-name>

This will give the following output

      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
    100  6354  100  6354    0     0  20901      0 --:--:-- --:--:-- --:--:-- 20901
    Checking if TLJH is already installed...
    Setting up hub environment
    Installed python & virtual environment
    Set up hub virtual environment
    Setting up TLJH installer...

Then after a couple of minutes

    Setup tljh package
    Starting TLJH installer...
    Setting up admin users
    Granting passwordless sudo to JupyterHub admins...
    Setting up user environment...
    Downloading & setting up user environment...
    Setting up JupyterHub...
    Downloading traefik 1.7.18...
    Created symlink /etc/systemd/system/multi-user.target.wants/jupyterhub.service → /etc/systemd/system/jupyterhub.service.
    Created symlink /etc/systemd/system/multi-user.target.wants/traefik.service → /etc/systemd/system/traefik.service.
    Waiting for JupyterHub to come up (1/20 tries)
    Waiting for JupyterHub to come up (2/20 tries)
    Done!

The JupyterHub server is now running.

> Note that if you have not configured your server's [firewall](#firewall), anyone can gain root access to your server using the web interface!

## Setting Up TLS

[source](http://tljh.jupyter.org/en/latest/howto/admin/https.html)

At this stage you need to disable the firewall allowing HTTP connections, so before that, set up a random password for the [dummy authenticator](http://tljh.jupyter.org/en/latest/howto/auth/dummy.html)

    tljh-config set auth.DummyAuthenticator.password <secure random password>
    tljh-config set auth.type dummyauthenticator.DummyAuthenticator
    tljh-config reload

Disable the firewall

    ufw reset

> It might be necessary to restart the server
>
>     reboot

> I have noticed that using a domain name to access the jupyterhub at this stage will not spawn the jupyter server after logging in... Use the IP address of your server at this stage

Now enable HTTPS from letsencrypt

    tljh-config set https.enabled true
    tljh-config set https.letsencrypt.email <email>
    tljh-config add-item https.letsencrypt.domains <domain>

> You can add another domain for the HTTPS, for instance www.domain by running the last command again with the other domain

You can see your tljh config with the following command

    tljh-config show

Sample output

    users:
      admin:
      - admin
    auth:
      DummyAuthenticator:
        password: ...
      type: dummyauthenticator.DummyAuthenticator
    https:
      enabled: true
      letsencrypt:
        email: ...
        domains:
        - example.com
        - www.example.com

Now just reload the proxy to get the certificates

    tljh-config reload proxy

> You can see the proxy logs with
>
>     journalctl -f -u traefik

Now access the jupyterhub website using your domain

## Google OAuthenticator

[source](http://tljh.jupyter.org/en/latest/howto/auth/google.html)

### Creating OAuth Tokens

- Go to console.cloud.google.com
- Create or select a project
- If you have not setup OAuth before:

  - Open APIs & Services -> OAuth consent screen
  - Fill the form
  - Insert `yourjupyterhub` (and the other addresses like `www.yourjupyterhub`) to `Authorized domains`
  - Insert `https://\<yourjupyterhub>` to `Application Homepage link`
  - Wait for a few days

- After OAuth consent is set up create new OAuth credentials

  - APIs & Services -> Credentials
  - Create credentials -> OAuth client ID
  - Select Web application
  - Inserst your jupyterhub's HTTPS address to `Authorized JavaScript origins`
  - Inserst your `<HTTPS address>/hub/oauth_callback` to `Authorized redirect URIs`

Configure tljh to use GoogleOAuthenticator

    tljh-config set auth.GoogleOAuthenticator.client_id '<my-tljh-client-id>'
    tljh-config set auth.GoogleOAuthenticator.client_secret '<my-tljh-client-secret>'
    tljh-config set auth.GoogleOAuthenticator.oauth_callback_url 'https://<yourjupyterhub>/hub/oauth_callback'
    tljh-config set auth.type oauthenticator.google.GoogleOAuthenticator
    tljh-config reload

> After setting up GoogleOAuthenticator the DummyAuthenticator can not be used, which means you can't login using the admin username and password from the web ui

> Initially anyone can login to jupyterhub as a student, which may not be ideal. See [configuring users](#configuring-users)

## Configuring Users

### Configuring Admins

[source](http://tljh.jupyter.org/en/latest/howto/admin/admin-users.html)

Configure your google mail address to have admin privileges

    tljh-config add-item users.admin <your google email>
    tljh-config reload

> You can see the config (located at `/opt/tljh/config/config.yaml`) with
>
>     tljh-config show

### Configuring Allowed Users

By default anyone can login to jupyterhub as a student.

Add yourself as an allowed user

    tljh-config add-item users.allowed <your google email>
    tljh-config reload

Having the allowed list in the config disables login for old and new accounts. They will see a 403 Forbidden screen after logging in.

### Admin Control Panel

Log in and click `Control Panel` on the top right, then click `Admin`.

Adding users from here does not add them to the tljh config file, but allows them to login (they are added to another whitelist).

## Nbgrader

### Installation

[source](https://nbgrader.readthedocs.io/en/stable/user_guide/installation.html)

Log in to jupyterhub as any admin user

Open a terminal `New -> Terminal`

> This will create a new shell in the jupyter python environment,
> Now you have to commands like `jupyter` and `conda`

    sudo pip install nbgrader
    sudo jupyter nbextension install --sys-prefix --py nbgrader --overwrite
    sudo jupyter nbextension enable --sys-prefix --py nbgrader
    sudo jupyter serverextension enable --sys-prefix --py nbgrader

You need to restart the single-user server (systemd daemon) or reboot the server

    systemctl restart jupyter-<username>

Now you can access the `Formgrader` page

#### Creating the Exchange Directory

Nbgrader requires a directory which everyone can access, to share assignments and fetch submissions.

Create the directory (you can run this in the web shell or through SSH)

    sudo mkdir -m 777 -p /srv/nbgrader/exchange

#### Creating the Global Nbgrader Config & The Course

[source](https://nbgrader.readthedocs.io/en/stable/configuration/jupyterhub_config.html)

This file is used to specify the course id and its location.

This nbgrader configuration only supports one course, so select a name for it.

Then create the following file (requires superuser privileges)

    $ cat /opt/tljh/user/etc/jupyter/nbgrader_config.py
    c = get_config()
    c.CourseDirectory.course_id = '<course_name>'
    c.CourseDirectory.root = '/home/jupyter-<username>/<course_name>'

> The warnings from the formgrader UI should have disappeared

Create a new sample course (using the web shell) with

    nbgrader quickstart <course_name>

> Your files should now contain the course folder with `source`, `gradebook.db` and `nbgrader_config.py` in it.
>
> Accessing the formgrader now should show an assignment called `ps1`.
